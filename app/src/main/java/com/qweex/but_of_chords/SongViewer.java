package com.qweex.but_of_chords;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.StringReader;
import java.net.URLEncoder;

public class SongViewer extends Activity {

    WebView webView;
    Song song;
    final String ENCODING = "utf-8";
    Outputter pretty = new ModifiedHTMLOutputter(),
              html = new ModifiedHTMLOutputter(),
              chordPro = new ChordProOutputter(),
              lyrics = new LyricOutputter();
    Outputter[] formats = new Outputter[] {html, pretty, lyrics};
    String[] mimes = new String[] {"text/plain", "text/html", "text/plain"};

    final static int
        RAW_HTML = 0,
        CHORDS = 1,
        LYRIC = 2,
        EDIT = 3,
        VIEW = 4;

    int currentMode = CHORDS;

    class ModifiedHTMLOutputter extends HTMLOutputter {
        @Override
        public String chord(Song.Chord line) {
            return "<chord onclick='chord.show(this.innerHTML);'>" + line.transpose(song.key) + "</chord>";
        }

        @JavascriptInterface
        public void show(String chord) {
            Toast.makeText(SongViewer.this, chord, Toast.LENGTH_SHORT).show();

            ChordView chordView = new ChordView(SongViewer.this, new int[] {3,2,2,1});

            TextView name = new TextView(SongViewer.this);
            name.setText(chord);
            name.setTextSize(chordView.px(24));

            LinearLayout ll = new LinearLayout(SongViewer.this);
            ll.addView(name);
            ll.setOrientation(LinearLayout.VERTICAL);
            ll.addView(chordView);
            ll.setGravity(Gravity.CENTER_HORIZONTAL);

            AlertDialog dialog = new AlertDialog.Builder(SongViewer.this)
                    .setView(ll)
                    .create();
            dialog.show();
            dialog.getWindow().setLayout(
                    chordView.px(250),
                    chordView.px(400)
            );
            dialog.show();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        song = MainActivity.chords.get(getIntent().getIntExtra("position", 0));

        webView = new WebView(this);
        WebSettings ws = webView.getSettings();
        ws.setJavaScriptEnabled(true);
        webView.addJavascriptInterface(pretty, "chord");
        webView.loadDataWithBaseURL(null, formats[currentMode].song(song), mimes[currentMode], ENCODING, null);
        setContentView(webView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, EDIT, 0, "Edit");
        Menu mode = menu.addSubMenu(0, VIEW, 0, "View");
        mode.setGroupCheckable(0, true, true);
        mode.add(0, CHORDS, 0, "Chords");
        mode.add(0, LYRIC, 0, "Lyrics");
        mode.add(0, RAW_HTML, 0, "HTML");
        mode.getItem(currentMode).setChecked(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==EDIT) {
            Intent viewer = new Intent(SongViewer.this, SongEditor.class);
            viewer.putExtra("title", song.title());
            viewer.putExtra("text", chordPro.song(song));
            viewer.putExtra("filename", MainActivity.filenames.get(song));
            Log.d("Text", viewer.getStringExtra("text"));
            startActivityForResult(viewer, 0);
            return super.onOptionsItemSelected(item);
        }
        switch(item.getItemId()) {
            case RAW_HTML:
            case LYRIC:
            case CHORDS:
                currentMode = item.getItemId();
                String songContent = formats[currentMode].song(song);
                if(currentMode!=CHORDS)
                    songContent = URLEncoder.encode(songContent).replaceAll("\\+", " ");
                Log.d("song", songContent);
                webView.loadData(songContent, mimes[currentMode], ENCODING);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK && data.getStringExtra("text")!=null) {
            song.update(new StringReader(data.getStringExtra("text")));
            webView.loadData(formats[currentMode].song(song), mimes[currentMode], ENCODING);
        }
    }
}
