package com.qweex.but_of_chords;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class SongEditor extends Activity {
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(getIntent().getStringExtra("filename"));

        editText = new EditText(this);
//        editText.setInputType(
//                InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT
//        );
        editText.setSingleLine(false);
        editText.setGravity(Gravity.TOP | Gravity.LEFT);
        editText.setHorizontallyScrolling(true);
        editText.setVerticalScrollBarEnabled(true);
//        editText.setImeOptions(EditorInfo.IME_FLAG_NAVIGATE_NEXT);
        editText.setText(getIntent().getStringExtra("text"));

        FrameLayout frame = new FrameLayout(this);
        frame.addView(editText, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT
        ));

        setContentView(frame);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if(editText.getText().toString().equals(getIntent().getStringExtra("text")))
                return super.onKeyDown(keyCode, event);

            new AlertDialog.Builder(this)
                    .setTitle("Discard changes?")
                    .setMessage("Any changes you made will be lost.")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .show();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, "Save");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent data = new Intent();
//        data.putExtra("filename", getIntent().getStringExtra("filename"));
        data.putExtra("text", editText.getText().toString());

        try {
            File outputFile = new File(MainActivity.SD_DIR, getIntent().getStringExtra("filename"));
            FileWriter out = new FileWriter(outputFile);
            out.write(editText.getText().toString());
            out.close();
            setResult(RESULT_OK, data);
            finish();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Failed to write to file: " + getIntent().getStringExtra("filename"), Toast.LENGTH_LONG)
            .show();
        }
        return super.onOptionsItemSelected(item);
    }
}
