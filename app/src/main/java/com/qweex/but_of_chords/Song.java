package com.qweex.but_of_chords;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Song {
    String transposedKey;
    List<LineBase> lines = new ArrayList<LineBase>();
    String title, subtitle, key;

    public enum Directive {
        TITLE("title", "t", true),
        SUBTITLE("subtitle", "su", true),
        START_OF_CHORDS("start_of_chords", "soc", false),
        END_OF_CHORDS("end_of_chords", "eoc", false),
        COMMENT("comment", "c", true),
        START_OF_TAB("start_of_tab", "sot", false),
        END_OF_TAB("end_of_tab", "eot", false),
        GUITAR_COMMENT("guitar_comment", "gc", true),
        KEY("key", "k", true);

        private final String name, sname;
        private boolean arg;
        Directive(final String n, final String s, boolean a) { name = n; sname = s; arg = a; }
        @Override
        public String toString() { return name; }
        public String alt() { return sname; }
        public boolean hasArg() { return arg; }

        public boolean isStart() { return name.startsWith("start_of_"); }
        public boolean isEnd() { return name.startsWith("end_of_"); }
        public String getTag() {
            if(isStart()) return name.substring("start_of_".length());
            if(isEnd()) return name.substring("end_of_".length());
            return null;
        }
    }

    public enum Note {
        C, Db, D, Eb, E, F, Gb, G, Ab, A, Bb, B;

        public Note transpose(Note fromKey, Note toKey) {
            int iAm = -1, fromIs = -1, toIs = -1;
            for(int i=0; i< Note.values().length; i++) {
                if (Note.values()[i].equals(this))
                    iAm = i;
                if (Note.values()[i].equals(fromKey))
                    fromIs = i;
                if (Note.values()[i].equals(toKey))
                    toIs = i;
            }
            if(iAm==-1 || fromIs==-1 || toIs==-1)
                throw new RuntimeException("Unable to transpose " + this + " from " + fromKey + " to " + toKey);
            return Note.values()[
                    (((iAm - fromIs) % Note.values().length) + toIs) % Note.values().length
            ];
        }

        public Note transpose(int adj) {
            int meI = -1;
            for(int i=0; i< Note.values().length; i++)
                if(Note.values()[i].equals(this)) {
                    meI = i;
                    break;
                }
            //if(meI==-1) throw "This should literally be impossible";
            meI = meI + adj;
            if(meI<0)
                meI += Note.values().length;
            else if(meI > Note.values().length)
                meI = meI % Note.values().length;
            return Note.values()[meI];
        }
    }


    final static Pattern CHORD_REGEX, DIRECTIVE_REGEX, COMMENT_REGEX;

    static {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for(Directive d : Directive.values()) {
            if(!first)
                sb.append("|");
            sb.append(d).append("|").append(d.alt());
            first = false;
        }
        DIRECTIVE_REGEX = Pattern.compile("\\{(" + sb.toString() + ")[: ]*(.*)\\}");
        Log.d("DIRECTIVE_REGEX", DIRECTIVE_REGEX.pattern());

        CHORD_REGEX = Pattern.compile("\\[([A-G])(#|b)?(m|dim|maj|sus)?([0-9])?(/([A-G])(#|b)?)?\\]");
        COMMENT_REGEX = Pattern.compile("#\\w+(.*)");
    }

    public Song(Reader reader) {
        update(reader);
    }

    public void update(Reader r) {
        BufferedReader br = new BufferedReader(r);
        lines.clear();
        String str;
        try {
            while((str = br.readLine()) != null) {
                if(str.trim().length()==0)
                    lines.add(new LineRaw());
                else if(str.matches(COMMENT_REGEX.pattern()))
                    lines.add(new LineComment(str));
                else if(str.matches(DIRECTIVE_REGEX.pattern())) {
                    LineDirective directive = new LineDirective(str);
                    lines.add(directive);
                    if(directive.getDirective().equals(Directive.TITLE))
                        title = directive.arg;
                    else if(directive.getDirective().equals(Directive.SUBTITLE))
                        subtitle = directive.arg;
                    else if(directive.getDirective().equals(Directive.KEY))
                        key = directive.arg;
                    else if(directive.getDirective().equals(Directive.START_OF_TAB)) {
                        boolean closedSuccessfully = false;
                        while((str = br.readLine()) != null) {
                            if(str.matches(DIRECTIVE_REGEX.pattern())) {
                                directive = new LineDirective(str);
                                if(directive.getDirective().equals(Directive.END_OF_TAB)) {
                                    closedSuccessfully = true;
                                    lines.add(directive);
                                    break;
                                }
                            }
                            lines.add(new LineRaw(str));
                        }
                        if(!closedSuccessfully)
                            throw new RuntimeException("start_of_tag without end_of_tag");
                    }
                } else
                    lines.add(new Line(str));
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String title() {
        return title;
    }

    public String subtitle() {
        return subtitle;
    }

    public void transpose(String newKey) {
        transposedKey = newKey;
    }

    class Chord {
        Note note;              // 1
        String  sharpFlat,      // 2
                modifier,       // 3
                digit,          // 4
                secondNote,     // 5.1
                secondSharpFlat;// 5.2

        public Chord(String str) {
            Matcher matcher = CHORD_REGEX.matcher(str);
            matcher.find();
            note = Note.valueOf(matcher.group(1));
            sharpFlat = matcher.group(2);
            modifier = matcher.group(3);
            digit = matcher.group(4);
            secondNote = matcher.group(5+1);
            secondSharpFlat = matcher.group(5+2);
            //*
            matcher.reset();
            for (int i = 1; matcher.find(); i++) {
                Log.d("--Group[" + i + "]", matcher.start() + " = " + matcher.group());
            }//*/
        }

        public String toString() {
            return note + _toString();
        }

        private String _toString() {
            StringBuilder sb = new StringBuilder();
            if(sharpFlat!=null)
                sb.append(sharpFlat);
            if(modifier!=null)
                sb.append(modifier);
            if(digit!=null)
                sb.append(digit);
            if(secondNote!=null) {
                sb.append("/").append(secondNote);
                if(secondSharpFlat!=null)
                    sb.append(secondSharpFlat);
            }
            return sb.toString();
        }

        protected String transpose(String toKey) {
            if(key==null || key.equals(toKey))
                return toString();
            return key; //TODO: Transpose logic
        }
    }

    class Word {
        //
        String lyric;
        Chord chord;
        boolean latterSyllable;
        public Word(String str, boolean latterSyllable) {
            this.latterSyllable = latterSyllable;
            Matcher matcher = CHORD_REGEX.matcher(str);
            Log.d("New Word", str + " ? " + matcher.find());
            matcher.reset();
            if(matcher.find()) {
                lyric = str.substring(matcher.group().length());
                chord = new Chord(matcher.group());
            } else {
                lyric = str;
                Log.d("..Word", lyric);
            }
        }

        public Chord chord() { return chord; }

        public boolean hasChord() {
            return chord!=null;
        }
    }

    abstract class LineBase {}

    class Line extends LineBase {
        List<Word> words = new ArrayList<Word>();
        public Line(String line) {
            Log.d("New Line", line);
            for(String str : line.split(" ")) {
                Matcher matcher = CHORD_REGEX.matcher(str);
                Log.d("--Line", str);
                int lastStart = 0;
                boolean isNotFirst = false;
                while(matcher.find() && lastStart >= 0) {
//                    Log.d("..word..", matcher.start() + ">" + lastStart);
                    if(matcher.start()>lastStart) {
                        words.add( new Word(str.substring(lastStart, matcher.start()), isNotFirst) );
                        isNotFirst = true;
                    }
                    lastStart = matcher.start();
                }
                if(lastStart < str.length())
                    words.add( new Word(str.substring(lastStart, str.length()), isNotFirst) );
            }
        }

    }

    class LineDirective extends LineBase {
        String arg;
        Directive directive;

        public LineDirective(String str) {
            Matcher m = DIRECTIVE_REGEX.matcher(str);
            m.find();
            String directiveName = m.group(1);
            this.arg = m.group(2);
            Log.d("New Directive", directive + "|" + arg + " <- " + str);

            for(Directive d : Directive.values())
                if(directiveName.equals(d.toString()) || directiveName.equals(d.alt())) {
                    this.directive = d;
                    break;
                }
            if(this.directive==null)
                throw new RuntimeException ("Unable to match directive: " + directiveName);
        }

        public Directive getDirective() { return directive; }
    }

    class LineRaw extends LineBase {
        String raw;
        public LineRaw() { this(""); }
        public LineRaw(String s) {
            raw = s;
            Log.d("New Raw", raw);
        }
    }

    class LineComment extends LineBase {
        String comment = "";

        public LineComment(String str) {
            Log.d("New Comment", str);
            Matcher m = COMMENT_REGEX.matcher(str);
            if(m.find())
                comment = m.group(1);
        }
    }
}
