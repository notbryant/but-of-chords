package com.qweex.but_of_chords;

public class LyricOutputter extends Outputter{

    //TODO: Unicode characters not displayed in WebView

    @Override
    public String song(Song song) {
        StringBuilder sb = new StringBuilder();
        if(song.title()!=null) {
            sb.append(song.title()).append('\n');
            if (song.subtitle() != null)
                sb.append(song.subtitle()).append('\n');
            sb.append("---\n");
        }

        return sb.append(super.song(song))
                .toString()
                .replaceAll("\r", "\n")
                .replaceAll("\n\n\n+", "\n\n").trim();
    }

    @Override
    public String chord(Song.Chord line) {
        return "";
    }

    @Override
    public String word(Song.Word word) {
        return word.lyric;
    }

    @Override
    public String line(Song.Line line) {
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<line.words.size()-1; i++) {
            sb.append(word(line.words.get(i)));
            if(!line.words.get(i+1).latterSyllable)
                sb.append(' ');
        }
        sb.append(word(line.words.get(line.words.size()-1)));
        return sb.toString().trim();
    }

    @Override
    public String lineDirective(Song.LineDirective line) {
        return null;
    }

    @Override
    public String lineRaw(Song.LineRaw line) {
        if(line.raw.length()>0)
            return null;
        return line.raw;
    }

    @Override
    public String lineComment(Song.LineComment line) {
        return null;
    }
}
